
function loadXMLDoc() {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      myFunction(xmlhttp);
    }
  };
  xmlhttp.open("GET", "data.xml", true);
  xmlhttp.send();
}



var arr = [];
var filterArray = [];


function myFunction(xml) {
  var i;
  var xmlDoc = xml.responseXML;
    var titles = xmlDoc.getElementsByTagName("title");
    var ratings = xmlDoc.getElementsByTagName("rating");
    var providers = xmlDoc.getElementsByTagName("provider");
    var releases = xmlDoc.getElementsByTagName("released");
    
  
//  var x = xmlDoc.getElementsByTagName("title");
    
    
    for (i = 0; i <titles.length; i++) { 
        var obj = {"Title": titles[i].childNodes[0].nodeValue, "Rating": ratings[i].childNodes[0].nodeValue, "Provider": providers[i].childNodes[0].nodeValue, "Released": releases[i].childNodes[0].nodeValue};
        
        arr.push(obj);  
  }
    console.log(arr);
    drawTable(arr);
} 


function drawTable(arrayToDraw) {
    console.log(arrayToDraw);
    var table="<tr><th class='textleft'>Title</th><th class='textleft'>Rating</th><th class='textleft'>Provider</th><th class='textleft'>Release Date</th></tr>";
    for (i = 0; i <arrayToDraw.length; i++) { 
    table += "<tr><td>" +
    arrayToDraw[i].Title +
    "</td><td>" +
        arrayToDraw[i].Rating +
    "</td><td>" +
        arrayToDraw[i].Provider +
    "</td><td>" +
    formatDate(arrayToDraw[i].Released) +
    "</td></tr>";
  }
  document.getElementById("movielist").innerHTML = table;
    
}


function sortTitleAZ () {
    arr.sort(function(a, b) {
  var nameA = a.Title.toUpperCase(); 
  var nameB = b.Title.toUpperCase(); 
  if (nameA < nameB) {
    return 1;
  }
  if (nameA > nameB) {
    return -1;
  }

  // names must be equal
  return 0;
});
    drawTable(arr);
}


function sortTitleZA () {
    arr.sort(function(a, b) {
  var nameA = a.Title.toUpperCase(); 
  var nameB = b.Title.toUpperCase(); 
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }

  // names must be equal
  return 0;
});
    drawTable(arr);
}





function sortYearDesc () {
    arr.sort(function(a, b) {
  var nameA = a.Released; 
  var nameB = b.Released; 
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }

  // names must be equal
  return 0;
});
    drawTable(arr);
}


function sortYearAsc () {
    arr.sort(function(a, b) {
  var nameA = a.Released; 
  var nameB = b.Released; 
  if (nameA < nameB) {
    return 1;
  }
  if (nameA > nameB) {
    return -1;
  }

  // names must be equal
  return 0;
});
    drawTable(arr);
}






function filterBy (rating) {
    filterArray = [];
    for (var i = 0; i <arr.length; i++) { 
    if (arr[i].Rating == rating) {
        filterArray.push(arr[i]);
        console.log(arr[i]);
    }
        console.log(arr[i].Rating);
        console.log(rating);
        
  }
    console.log(filterArray);
    drawTable(filterArray);
}


function filterByProvider (provider) {
    filterArray = [];
    for (var i = 0; i <arr.length; i++) { 
    if (arr[i].Provider == provider) {
        filterArray.push(arr[i]);
        console.log(arr[i]);
    }
        
        
  }
    
    drawTable(filterArray);
}











function dropdownButtonRating() {
    document.getElementById("dropdownRating").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}


function dropdownButtonProvider() {
    document.getElementById("dropdownProvider").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}


function formatDate(date) {
    var formattedDate = "";
    switch(date.slice(5,7)){
        case "01" :
            formattedDate+="January";
            break;
        case "02" :
            formattedDate+="February";
            break;
        case "03" :
            formattedDate+="March";
            break;
        case "04" :
            formattedDate+="April";
            break;
        case "05" :
            formattedDate+="May";
            break;
        case "06" :
            formattedDate+="June";
            break;
        case "07" :
            formattedDate+="July";
            break;
        case "08" :
            formattedDate+="August";
            break;
        case "09" :
            formattedDate+="September";
            break;
        case "10" :
            formattedDate+="October";
            break;
        case "11" :
            formattedDate+="November";
            break;
        case "12" :
            formattedDate+="December";
            break;
            
    }
    var day = date.slice(8,10)
    var year = date.slice(0,4)
    
    return formattedDate+ " " + day + "," + " " + year;
}









